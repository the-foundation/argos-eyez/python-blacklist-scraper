import lxml
from lxml import html
import random

import datetime
import pytz
from datetime import date
import time
import os
import requests

import socket

urllib_ver2=0
try:
    from urllib.parse import urlparse, urlencode
    from urllib.request import urlopen, Request,build_opener
    from urllib.error import HTTPError
    import urllib
except ImportError:
    import urllib2
    from urlparse import urlparse
    from urllib import urlencode
    from urllib2 import urlopen, Request, HTTPError
    urllib_ver2=1

import user_agent_list as useragents

global_debug=0

import base64
def base64_encode(string):
    """
    Removes any `=` used as padding from the encoded string.
    """
    bytez = string.encode("utf-8")
    encoded=base64.urlsafe_b64encode(bytez).decode("utf-8")
    return encoded.rstrip("=")


def base64_decode(string):
    """
    Adds back in the required padding before decoding.
    """
    padding = 4 - (len(string) % 4)
    string = string + ("=" * padding)
    return base64.urlsafe_b64decode(string.encode("utf-8"))


builtin_rbls = [
    'b.barracudacentral.org',
#    'bl.spamcannibal.org',
    'bl.spamcop.net',
    'blacklist.woody.ch',
    'cbl.abuseat.org',
#       'cdl.anti-spam.org.cn',
    'combined.abuse.ch',
    'combined.rbl.msrbl.net',
    'db.wpbl.info',
    'dnsbl-1.uceprotect.net',
    'dnsbl-2.uceprotect.net',
    'dnsbl-3.uceprotect.net',
    'dnsbl.cyberlogic.net',
    'dnsbl.sorbs.net',
    'drone.abuse.ch',
    'drone.abuse.ch',
    'duinv.aupads.org',
    'dul.dnsbl.sorbs.net',
    'dul.ru',
    'dyna.spamrats.com',
    'dynip.rothen.com',
    'http.dnsbl.sorbs.net',
    'images.rbl.msrbl.net',
    'ips.backscatterer.org',
    'ix.dnsbl.manitu.net',
    'korea.services.net',
    'misc.dnsbl.sorbs.net',
    'noptr.spamrats.com',
    'ohps.dnsbl.net.au',
    'omrs.dnsbl.net.au',
    'orvedb.aupads.org',
    'osps.dnsbl.net.au',
    'osrs.dnsbl.net.au',
    'owfs.dnsbl.net.au',
    'pbl.spamhaus.org',
    'phishing.rbl.msrbl.net',
    'probes.dnsbl.net.au',
    'proxy.bl.gweep.ca',
    'rbl.interserver.net',
    'rdts.dnsbl.net.au',
    'relays.bl.gweep.ca',
    'relays.nether.net',
    'residential.block.transip.nl',
    'ricn.dnsbl.net.au',
    'rmst.dnsbl.net.au',
    'smtp.dnsbl.sorbs.net',
    'socks.dnsbl.sorbs.net',
    'spam.abuse.ch',
    'spam.dnsbl.sorbs.net',
    'spam.rbl.msrbl.net',
    'spam.spamrats.com',
    'spamrbl.imp.ch',
    't3direct.dnsbl.net.au',
#    'tor.dnsbl.sectoor.de',
#    'torserver.tor.dnsbl.sectoor.de',
    'ubl.lashback.com',
    'ubl.unsubscore.com',
    'virus.rbl.jp',
    'virus.rbl.msrbl.net',
    'web.dnsbl.sorbs.net',
    'wormrbl.imp.ch',
    'xbl.spamhaus.org',
    'zen.spamhaus.org',
    'zombie.dnsbl.sorbs.net',
    'b.barracudacentral.org',
    'cbl.abuseat.org',
    'http.dnsbl.sorbs.net',
    'misc.dnsbl.sorbs.net',
    'socks.dnsbl.sorbs.net',
    'web.dnsbl.sorbs.net',
    'dnsbl-1.uceprotect.net',
    'dnsbl-3.uceprotect.net',
    'sbl.spamhaus.org',
    'zen.spamhaus.org',
    'psbl.surriel.com',
#    'dnsbl.njabl.org',
    'rbl.spamlab.com',
    'noptr.spamrats.com',
#    'cbl.anti-spam.org.cn',
    'dnsbl.inps.de',
    'httpbl.abuse.ch',
    'korea.services.net',
    'virus.rbl.jp',
    'rbl.suresupport.com',
    'ips.backscatterer.org',
    'opm.tornevall.org',
    'multi.surbl.org',
    'tor.dan.me.uk',
    'relays.mail-abuse.org',
    'rbl-plus.mail-abuse.org',
    'access.redhawk.org',
    'rbl.interserver.net',
    'bogons.cymru.com',
    'bl.spamcop.net',
    'dnsbl.sorbs.net',
    'dul.dnsbl.sorbs.net',
    'smtp.dnsbl.sorbs.net',
    'spam.dnsbl.sorbs.net',
    'zombie.dnsbl.sorbs.net',
    'dnsbl-2.uceprotect.net',
    'pbl.spamhaus.org',
    'xbl.spamhaus.org',
    'ubl.unsubscore.com',
#    'combined.njabl.org',
    'dyna.spamrats.com',
    'spam.spamrats.com',
#    'cdl.anti-spam.org.cn',
    'drone.abuse.ch',
    'dul.ru',
    'short.rbl.jp',
    'virbl.bit.nl',
    'dsn.rfc-ignorant.org',
    'dsn.rfc-ignorant.org',
    'netblock.pedantic.org',
    'ix.dnsbl.manitu.net',
    'rbl.efnetrbl.org',
    'blackholes.mail-abuse.org',
    'dnsbl.dronebl.org',
    'db.wpbl.info',
#    'query.senderbase.org',
#    'bl.emailbasura.org',
    'combined.rbl.msrbl.net',
    'multi.uribl.com',
    'black.uribl.com',
#    'cblless.anti-spam.org.cn',
#    'cblplus.anti-spam.org.cn',
#    'blackholes.five-ten-sg.com',
    'sorbs.dnsbl.net.au',
    'rmst.dnsbl.net.au',
    'dnsbl.kempt.net',
    'blacklist.woody.ch',
#    'rot.blackhole.cantv.net',
    'virus.rbl.msrbl.net',
    'phishing.rbl.msrbl.net',
    'images.rbl.msrbl.net',
    'spam.rbl.msrbl.net',
    'spamlist.or.kr',
    'dnsbl.abuse.ch',
#    'bl.deadbeef.com',
    'ricn.dnsbl.net.au',
    'forbidden.icm.edu.pl',
    'probes.dnsbl.net.au',
    'ubl.lashback.com',
    'ksi.dnsbl.net.au',
    'uribl.swinog.ch',
    'bsb.spamlookup.net',
    'dob.sibl.support-intelligence.net',
#    'url.rbl.jp',
    'dyndns.rbl.jp',
    'omrs.dnsbl.net.au',
    'osrs.dnsbl.net.au',
    'orvedb.aupads.org',
    'relays.nether.net',
    'relays.bl.gweep.ca',
    'relays.bl.kundenserver.de',
    'dialups.mail-abuse.org',
    'rdts.dnsbl.net.au',
    'duinv.aupads.org',
    'dynablock.sorbs.net',
    'residential.block.transip.nl',
#    'dynip.rothen.com',
    'dul.blackhole.cantv.net',
    'mail.people.it',
    'blacklist.sci.kun.nl',
    'all.spamblock.unit.liu.se',
    'spamguard.leadmon.net',
    '53.8.8.8.8.ip-port.exitlist.torproject.org',
    '53.9.9.9.9.ip-port.exitlist.torproject.org',
    '53.1.1.1.1.ip-port.exitlist.torproject.org',
    'csi.cloudmark.com'
]


###deduplicate###
builtin_rbls = list(dict.fromkeys(builtin_rbls))


def CachedContents(url , timespan=1800 ):
    """
    pulls url if there is no local cache newer than timespan
    """
    statuscode=0
    retcode=0
    dstFile='/tmp/.url_cache_' + base64_encode(url)
    threshold_time =  pytz.utc.localize(datetime.datetime.fromtimestamp(time.mktime(time.localtime(time.time()-timespan))))
    try:
        
        file_time_pre = pytz.utc.localize(datetime.datetime.fromtimestamp(os.path.getmtime(dstFile)))
    except: 
        ## no file , the date is 1970-01-01
        file_time_pre=pytz.utc.localize(datetime.datetime.fromtimestamp(time.mktime(time.localtime(0))))
        pass
    if global_debug==1:
        ### we have time-offset aware objects like this : datetime.datetime(2020, 4, 21, 4, 30, 6, tzinfo=tzutc())
        print ("cache_time_pre: threshold :" , threshold_time.strftime("%d-%b-%Y (%H:%M:%S.%f)") , " local",file_time_pre.strftime("%d-%b-%Y (%H:%M:%S.%f)"))
    try:
        if ( file_time_pre < threshold_time ): ## cached file not present or older than threshold
            r = requests.head(url, headers={'User-Agent': random.choice(useragents.user_agent_list) })
            try:
                url_time = r.headers['last-modified']
                url_date = parsedate(url_time)
            except: ### no headers , set 10 minutes age , throttling dl
                if global_debug==1:
                    print('HeAders failed: ',url)
                retcode = r.status_code
                ##### get timezone aware datetime object from 600 sec ago
                url_date=pytz.utc.localize(datetime.datetime.fromtimestamp(time.mktime(time.localtime(time.time()-600))))
                #url_date=pytz.utc.localize(datetime.datetime.fromtimestamp(time.mktime(time.localtime(time.time()))))
                pass    
            retcode = r.status_code
            try:
                ##### get timezone aware datetime object
    #            file_time = pytz.utc.localize(date.fromtimestamp(os.path.getmtime(dstFile)))
                file_time = pytz.utc.localize(datetime.datetime.fromtimestamp(os.path.getmtime(dstFile)))
            except:
                ## no file , so 1970-01-01
                file_time=pytz.utc.localize(datetime.datetime.fromtimestamp(time.mktime(time.localtime(0))))
                pass
            # 10 minutes
            cache_time=timespan
            if url_date > file_time :
                try:
                    request = Request(url, headers={ 'User-Agent': random.choice(useragents.user_agent_list) })
                    if global_debug==1:
                        print ("Downloading :",url)
                    if urllib_ver2 == 1:
                        #print("urllib2 build opener")
                        opener=urllib2.build_opener()
                    else:
                        #print("urllib.opener v3")
                        opener=urllib.request.build_opener()
                    opener.addheaders = [('User-Agent',random.choice(useragents.user_agent_list))]
                    opened_request = opener.open(request,timeout=15)
                    ##statuscode=opened_request.code
                    if global_debug==1:
                        print ("STATUS_CODE: ",opened_request.code)
                    if opened_request.code == 200:
                        if global_debug==1:
                            print ("start_cache_write: ",dstFile)
                        cacheout = open(dstFile, "wb+")
                        cacheout.write(opened_request.read())
                        cacheout.close()
                    else:
                        return  "ERROR : " + str(e) + ' ' + url     
                except Exception as e:
                    if global_debug==1:
                        print ("ERROR : ",e,url)
                    return  "ERROR : " + str(e) + ' ' + url
            else:
                if global_debug==1:
                    ### we have time-offset aware objects like this : datetime.datetime(2020, 4, 21, 4, 30, 6, tzinfo=tzutc())
                    print ("cache_time: orig :" , url_date.strftime("%d-%b-%Y (%H:%M:%S.%f)") , " local",file_time.strftime("%d-%b-%Y (%H:%M:%S.%f)"))
        else:
            ###we use the cache and it is newer than 10 minute , so we pretend request worked 
            retcode=200    
#    url_content = opened_request.read()
        if global_debug==1:
            print ("start_cache_open : ",dstFile)
        cachefile = open(dstFile, "r")
        url_content = cachefile.read()
        if global_debug==1:
            print ("CACHED LEN: ",len(url_content))
        cachefile.close()
        #retcode = opened_request.code
        return url_content
    except Exception as e:
        if global_debug==1:
            print('SOMETTHING FAILED while getting http')
            print ("ERROR : ",e,url)
        raise ErrorDuringImport




###---

def get_rbl():
    rbls=[];
    tree=html.fromstring(CachedContents('http://multirbl.valli.org/list/'))
    for tr in tree.xpath('//*[@id="lo-main"]/table[1]//tr'):
        if tr[6].text_content() == 'b' : ###filter informational lists
            rbls.append(tr[2].text_content())
    rbls=rbls + builtin_rbls
    rbls = list(dict.fromkeys(rbls))
    return rbls


def get_rbl_domain():
    rbls=[];
    tree=html.fromstring(CachedContents('http://multirbl.valli.org/list/'))
    for tr in tree.xpath('//*[@id="lo-main"]/table[1]//tr'):
        if tr[5].text_content() == 'dom' :
            if tr[6].text_content() == 'b' : ###filter informational lists
                if not tr[2].text_content() == "(hidden)":
                    rbls.append(tr[2].text_content())            
    #rbls=rbls + builtin_rbls
    #rbls = list(dict.fromkeys(rbls))
    return rbls


def get_rbl_ipv4():
    rbls=[];
    tree=html.fromstring(CachedContents('http://multirbl.valli.org/list/'))
    for tr in tree.xpath('//*[@id="lo-main"]/table[1]//tr'):
        if tr[3].text_content() == 'ipv4' :
            if tr[6].text_content() == 'b' : ###filter informational lists
                if not tr[2].text_content() == "(hidden)":
                    rbls.append(tr[2].text_content())            
    #rbls=rbls + builtin_rbl_ipv6
    #rbls = list(dict.fromkeys(rbls))
    return rbls

def get_rbl_ipv6():
    rbls=[];
    tree=html.fromstring(CachedContents('http://multirbl.valli.org/list/'))
    for tr in tree.xpath('//*[@id="lo-main"]/table[1]//tr'):
        if tr[4].text_content() == 'ipv6' :
            if tr[6].text_content() == 'b' : ###filter informational lists
                if not tr[2].text_content() == "(hidden)":
                    rbls.append(tr[2].text_content())            
    #rbls=rbls + builtin_rbls_ipv6
    #rbls = list(dict.fromkeys(rbls))
    return rbls


def get_rbl_fullinfo():
    rbls_full={};
    tree=html.fromstring(CachedContents('http://multirbl.valli.org/list/'))
    for tr in tree.xpath('//*[@id="lo-main"]/table[1]//tr'):
        rbls_full[tr[2].text_content()]= { "url": tr[1].xpath(".//a/@href")[0] ,  "descr": tr[1].text_content() }
    return rbls_full

tree=html.fromstring(CachedContents('http://multirbl.valli.org/list/'))


#alive=tree.xpath("/html/body/*[@id='lo-main']//*[    preceding-sibling::h2[contains(.,'alive')]   and following-sibling::h2[contains(.,'dead')]]")[0]
#alive=tree.xpath("/html/body/*[@id='lo-main']//*[    preceding-sibling::h2[contains(.,'alive')]   and following-sibling::h2[contains(.,'dead')]]")
#alive=tree.xpath('//*[@id="lo-main"]/table[1]')[0]
#import pprint
#pprint.pprint(alive)


#rbls_full={};
#rbls=[];#

#for tr in tree.xpath('//*[@id="lo-main"]/table[1]//tr'):
#	rbls_full[tr[2].text_content()]= { "url": tr[1].xpath(".//a/@href")[0] ,  "descr": tr[1].text_content() }
#	rbls.append(tr[2].text_content())
#	if global_debug==1:
#		print (tr[2].text_content(),tr[1].xpath(".//a/@href")[0],tr[1].text_content())
#        print(len(rbls))
#	print (tr[2].text_content(),lxml.html.tostring(tr[1]),tr[1].text_content())

#print(len(rbls))
#rbls=rbls + builtin_rbls
#rbls = list(dict.fromkeys(rbls))
#print(len(rbls))

#print rbls

###---











##


#pageContent=requests.get('http://multirbl.valli.org/list/')
#tree = html.fromstring(pageContent.content)

#f = open("/tmp/vallilist.html", "r")
#tree = html.fromstring(f.read())

#tree=html.fromstring(CachedContents('http://multirbl.valli.org/list/'))

#alive=tree.xpath('/html/body/div[3]/table[1]')
#alive=tree.xpath("/html/body/*[@id='lo-main']/h1/h2[contains(.,'alive')]//*")
#alive=tree.xpath("/html/body/*[@id='lo-main']/h1/h2[re:test(., 'alive', 'i')]//*")
#alive=tree.xpath("/html/body/*[@id='lo-main']//h2[contains(.,'alive')]")
#tree=html.fromstring(CachedContents('http://multirbl.valli.org/list/'))
#alive=tree.xpath("/html/body/*[@id='lo-main']//*[    preceding-sibling::h2[contains(.,'alive')]   and following-sibling::h2[contains(.,'dead')]]")[0]


#print alive;
#print([tr.text_content() for tr in alive.xpath('//tr')])
#print([{tr[2].text_content(),tr[1].text_content()} for tr in alive.xpath('//tr')])

#tree=html.fromstring(CachedContents('http://multirbl.valli.org/list/'))
#alive=tree.xpath("/html/body/*[@id='lo-main']//*[    preceding-sibling::h2[contains(.,'alive')]   and following-sibling::h2[contains(.,'dead')]]")[0]



#print([ tr.text_content() for tr in tree.xpath("/html/body/*[@id='lo-main']//*[    preceding-sibling::h2[contains(.,'alive')]   and following-sibling::h2[contains(.,'dead')]]/tr" ) )

#alive=tree.xpath("//*[    preceding-sibling::h2[. = 'Alive']   and following-sibling::h2[. = 'Dead']]")
#print alive




